from pyproj import Transformer
import json
from math import floor

"""
Utilise directement le fichier fourni avec la carte
"""
# with open("geometry.dat","r") as file:
#     data = json.load(file)

"""
Si le fichier n'est pas disponible, les données peuvent être copiées collées comme cela :
(J'ai enlevé celles qui sont inutiles
"""
data = {
  "epsgCarto": "EPSG:2154",
  "coordinatesCarto": [
    [
      648007.5514944869,
      6881310.412327921
    ]
  ],
  "altitudeZero": 40,
  "coordinatesGame": [
    [
      -614.4,
      614.4
    ]
  ]
}

"""
Rectificateur car la carte entière n'est pas centrée en zéro :
"""

rect_X = -12802
rect_Z = 13825

"""
Définition des fonctions utiles
"""

def conversion_loc_bloc(latitude,longitude,hauteur=32):
    transformer = Transformer.from_crs("epsg:4326", data["epsgCarto"])
    x_carto,z_carto = transformer.transform(latitude,longitude)
    translation_X = data["coordinatesGame"][0][0] - data["coordinatesCarto"][0][0]
    translation_Z = data["coordinatesGame"][0][1] - data["coordinatesCarto"][0][1]

    x = x_carto + translation_X + rect_X
    z = z_carto + translation_Z + rect_Z
    y = hauteur + data["altitudeZero"]

    print(f"/teleport {floor(x)} {floor(y)} {floor(z)}")
    return (floor(x),floor(z))


def conversion_bloc_loc(x,z,y=72):
    translation_X = data["coordinatesGame"][0][0] - data["coordinatesCarto"][0][0]
    translation_Z = data["coordinatesGame"][0][1] - data["coordinatesCarto"][0][1]

    x_carto = x - translation_X - rect_X
    z_carto = z - translation_Z - rect_Z

    transformer = Transformer.from_crs(data["epsgCarto"],"epsg:4326")
    latitude,longitude = transformer.transform(x_carto,z_carto)
    altitude = y - data["altitudeZero"]

    print(longitude,latitude,altitude)
    return (latitude,longitude)





"""
Fin de script qui demande les coordonnées en input, ça peut être utile éventuellement
Pour l'activer : il faut mettre continuer = True
"""
continuer = False


def input_decimal(texte):
    fin = False
    while not fin:
        n = input(texte)
        if n.lower() == "q":
            fin = True
        else:
            try:
                return float(n)
            except ValueError:
                print("La valeur attendu doit être un nombre réel, veuillez recommencer")


while continuer:
    p = input("Dans quel sens voulez vous effectuer la conversion ? (1:GPS->XYZ ; 2:XYZ->GPS)\nPour quitter mettez q\n")
    if p == "1" :
        longitude = input_decimal("Longitude : ")
        latitude = input_decimal("Latitude : ")
        hauteur = input_decimal("Hauteur (si vous ne voulez pas spécifier de hauteur mettez -1) : ")
        if hauteur == -1 :
            hauteur = 32
        conversion_loc_bloc(latitude,longitude,hauteur)
    elif p == "2":
        x = input_decimal("X : ")
        Y = input_decimal("Y : ")
        Z = input_decimal("Z : ")
        conversion_bloc_to_loc(x,z,y)
    elif p.lower() == "q":
        continuer = False
    else:
        print("Valeur non reconnue, veuillez réessayer")