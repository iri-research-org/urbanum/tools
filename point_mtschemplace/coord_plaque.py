import json
"""
Utilise directement le fichier fourni avec la carte
"""
#Fichier geometry.dat de la plaque a uploader
# with open("geometry.dat","r") as file:
#     data_plaque = json.load(file)
#Fichier geometry.dat de la carte complète de seine saint denis
# with open("geometry.dat","r") as file:
#     data_carte = json.load(file)
"""
Si le fichier n'est pas disponible, les données peuvent être copiées collées comme cela :
(J'ai enlevé celles qui sont inutiles)
"""

data_carte = {
  "epsgCarto": "EPSG:2154",
  "coordinatesCarto": [
    [
      648007.5514944869,
      6881310.412327921
    ]
  ],
  "altitudeZero": 40,
  "coordinatesGame": [
    [
      -614.4,
      614.4
    ]
  ]
}

data_plaque = {
  "epsgCarto": "EPSG:2154",
  "coordinatesCarto": [
    [
      655680.2036687452,
      6870825.112015195
    ],
    [
      655208.7306196466,
      6870825.112015195
    ],
    [
      655208.7306196466,
      6869455.854857473
    ]
  ],
  "coordinatesGame": [
    [
      -512.0,
      512.0
    ],
    [
      512.0,
      512.0
    ],
    [
      512.0,
      -512.0
    ],
    [
    -512.0,
    -512.0
    ]
  ]
}

"""
Rectificateur car la carte entière n'est pas centrée en zéro :
"""
rect_X = -12802
rect_Z = 13825

"""
Translation pour passer des coordonnées carto (EPSG 2154) en coordonnées X,Z
"""
translation_X = data_carte["coordinatesGame"][0][0] - data_carte["coordinatesCarto"][0][0]
translation_Z = data_carte["coordinatesGame"][0][1] - data_carte["coordinatesCarto"][0][1]


"""
Fonction à exécuter
"""
def bloc_to_place(hauteur = 512):
    if data_carte["epsgCarto"] != data_plaque["epsgCarto"]:
        return print("Cartes non compatibles via cette méthode")
    x_carto_centre_plaque = (data_plaque["coordinatesCarto"][0][0] + data_plaque["coordinatesCarto"][2][0])/2
    z_carto_centre_plaque = (data_plaque["coordinatesCarto"][0][1] + data_plaque["coordinatesCarto"][2][1])/2
    x = x_carto_centre_plaque + translation_X + data_plaque["coordinatesGame"][3][0] + rect_X
    z = z_carto_centre_plaque + translation_Z + data_plaque["coordinatesGame"][3][1] + rect_Z
    print(int(x),int(hauteur),int(z))

bloc_to_place()